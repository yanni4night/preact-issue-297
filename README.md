# preact-issue-297
[![Build with Panto][build-image]][build-url]

Replication of <https://github.com/developit/preact/issues/297>.

My environment:

 - Mac Chrome 52.0.2743.116 (64-bit)
 - Nodejs v6.5.0

I use [***panto***](https://github.com/pantojs/panto) to build, but that does not matter, use *webpack* if you love.

## First

You have to build the project:

```sh
git clone https://yanni4night@bitbucket.org/yanni4night/preact-issue-297.git
cd preact-issue-297
npm install
make build
```

## Second

Start http server:

```sh
make start
```

Open browser to <http://localhost:3000/>, you'will see a web page showing a item.

## Third

Click the item, and some text showed, then ***navigate back using "back" button of your browser, notice console in devtools***.

![](http://ww1.sinaimg.cn/large/801b780ajw1f7n4lk9ttoj21ds0cqq66.jpg)

[build-image]:https://img.shields.io/badge/build%20with-panto-yellowgreen.svg
[build-url]:http://pantojs.xyz/