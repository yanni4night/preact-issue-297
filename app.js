/**
 * Copyright (C) 2016 yanni4night.com
 * app.js
 *
 * changelog
 * 2016-09-08[13:24:41]:revised
 *
 * @author yanni4night@gmail.com
 * @version 0.1.0
 * @since 0.1.0
 */
'use strict';

var express = require('express');
var app = express();

app.use(express.static(__dirname + '/dist/'));

app.get('/api/topics/hot.json', function (req, res) {
    res.json(
        [{
            "id": 304804,
            "title": "Test hot title,click me",
            "content": "This is a test",
            "content_rendered": "This is a test<br/>, no navigate back using 'back' button of your browser, notice console",
            "replies": 194,
            "member": {
                "id": 188233,
                "username": "name",
                "avatar_mini": "http://dummyimage.com/60/60/fff&text=icon",
                "avatar_normal": "http://dummyimage.com/60/60/fff&text=icon",
                "avatar_large": "http://dummyimage.com/60/60/fff&text=icon"
            }
        }]);
});

app.get('/api/topics/latest.json', function (req, res) {
    res.json(
        [{
            "id": 304804,
            "title": "Test latest title, click me",
            "content": "This is a test",
            "content_rendered": "This is a test<br/>, no navigate back using 'back' button of your browser, notice console",
            "replies": 194,
            "member": {
                "id": 188233,
                "username": "name",
                "avatar_mini": "http://dummyimage.com/60/60/fff&text=icon",
                "avatar_normal": "http://dummyimage.com/60/60/fff&text=icon",
                "avatar_large": "http://dummyimage.com/60/60/fff&text=icon"
            }
        }]);
});

app.listen(3000);